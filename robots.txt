# Block robots
User-agent: *
Disallow: /assets/
Disallow: /assets/files
Disallow: /keys.html
Disallow: /keysigning.html
Allow: /index.html