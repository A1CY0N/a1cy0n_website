function encode(str) {
    key = Math.floor(Math.random() * (9 * Math.pow(10, 5 - 1))) + Math.pow(10, 5 - 1);
    var encodedStr = key.toString(16);
    var encodedString = encodedStr.padStart(5, "0");
    for (var n = 0; n < str.length; n++) {
        var charCode = str.charCodeAt(n);
        var encoded = charCode ^ key;
        encodedString += encoded.toString(16).padStart(5, "0");
    }
    return encodedString;
}