function decode(encoded) {
    var str = "";
    var keyInHex = encoded.substr(0, 5);
    var key = parseInt(keyInHex, 16);
    for (var n = 5; n < encoded.length; n += 5) {
        var charInHex = encoded.substr(n, 5)
        var char = parseInt(charInHex, 16);
        var output = char ^ key;
        str += String.fromCharCode(output);
    }
    return str;
}

var el = document.getElementsByClassName("protected");
var mailel = document.getElementsByClassName("mail-protected");

for (var i = 0; i < mailel.length; i++) {
    updateMail(mailel[i])
}

for (var i = 0; i < el.length; i++) {
    updateAnchor(el[i])
}

function updateMail(el) {
    var encoded = el.getAttribute("href");
    var decoded = decode(encoded);
    el.textContent = decoded.replace('mailto:', '');
}

function updateAnchor(el) {
    var encoded = el.getAttribute("href");
    var decoded = decode(encoded);
    el.href = decoded;
}