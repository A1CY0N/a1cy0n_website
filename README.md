# A1cy0n_website

> A1CY0N's personal website

<a href="https://512kb.club"><img src="https://512kb.club/images/green-team.svg" /></a>

Project to host the front-end of my site [a1cy0n.xyz](https://a1cy0n.xyz) with a nice terminal look. Feel free to reuse my code!

![](assets/images/logo.png)


## Meta

[@A1CY0N](https://mamot.fr/@a1c0n) – a1cy0n@tutanota.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/A1CY0N](https://gitlab.com/A1CY0N)

## Contributing

1. Fork it (<https://gitlab.com/A1CY0N/awesome_a1cy0n_website>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request